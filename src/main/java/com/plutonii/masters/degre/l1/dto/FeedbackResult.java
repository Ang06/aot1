package com.plutonii.masters.degre.l1.dto;

/**
 * Предопределенный набор возможных исходов анализа.
 */
public enum FeedbackResult {

    VERY_NEGATIVE("Очень негативный", 0.0, 1.0),
    NEGATIVE("Негативный", 1.0, 2.0),
    NEUTRAL("Нейтральный", 2.0, 3.0),
    POSITIVE("Позитивный", 3.0, 4.0),
    VERY_POSITIVE("Очень позитивный", 4.0, 5.0);

    private final String name;

    private final double minRang;

    private final double maxRang;

    FeedbackResult(String name, double minRang, double maxRang) {
        this.name = name;
        this.minRang = minRang;
        this.maxRang = maxRang;
    }

    public static FeedbackResult getResult(double score) {
        for (FeedbackResult result : FeedbackResult.values()) {
            if (score > result.minRang && score <= result.maxRang) {
                return result;
            }
        }

        throw new RuntimeException("Cannot find result for score: " + score);
    }
}
