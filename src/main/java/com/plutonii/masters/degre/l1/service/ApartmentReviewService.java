package com.plutonii.masters.degre.l1.service;

import com.plutonii.masters.degre.l1.dto.Review;
import com.plutonii.masters.degre.l1.repo.ReviewReader;

/**
 * Логика обработки - подготавливаем анализатор, читаем отзывы из файла и обрабатываем каждый по отдельности.
 */
public class ApartmentReviewService {
    public static void analyze() {
        NlpAnalyzer.init();
        var reviews = ReviewReader.readReviews().stream().map(Review::getText).toList();
        for (var review : reviews) {
            NlpAnalyzer.estimatingSentimentForReview(review);
        }
    }
}
