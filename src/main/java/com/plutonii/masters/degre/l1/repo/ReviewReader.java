package com.plutonii.masters.degre.l1.repo;

import com.google.gson.Gson;
import com.plutonii.masters.degre.l1.dto.Review;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import java.util.Objects;

/**
 * Класс для чтения отзывов из файла ресурсов
 */
public class ReviewReader {

    private final static String FILE_PATH = "/reviews.json";

    public static List<Review> readReviews() {
        try (Reader reader = new InputStreamReader(Objects.requireNonNull(ReviewReader.class.getResourceAsStream(FILE_PATH)))) {
            Review[] reviews = new Gson().fromJson(reader, Review[].class);
            System.out.println(reviews.length);
            return List.of(reviews);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
