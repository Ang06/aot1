package com.plutonii.masters.degre.l1.dto;

/**
 * DTO (Data Transfer Object) - класс для представления отдельного отзыва.
 */
public class Review {

    private String text;

    public Review() {
    }

    public Review(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public Review setText(String text) {
        this.text = text;
        return this;
    }
}
