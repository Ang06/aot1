package com.plutonii.masters.degre.l1.service;

import com.plutonii.masters.degre.l1.dto.FeedbackResult;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations.SentimentAnnotatedTree;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;

import java.util.Properties;

public class NlpAnalyzer {

    private static StanfordCoreNLP pipeline;

    /**
     * Инициализация и настройка анализатора.
     */
    public static void init() {
        Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, parse, sentiment");
        pipeline = new StanfordCoreNLP(props);
    }

    /**
     * Анализирует текст и выводи информацию по каждому предложению отдельно (для дебага)
     * @param text - текст для анализа
     */
    public static void estimatingSentiment(String text)
    {
        int sentimentInt;
        String sentimentName;
        Annotation annotation = pipeline.process(text);
        for(CoreMap sentence : annotation.get(CoreAnnotations.SentencesAnnotation.class))
        {
            Tree tree = sentence.get(SentimentAnnotatedTree.class);
            sentimentInt = RNNCoreAnnotations.getPredictedClass(tree);
            sentimentName = sentence.get(SentimentCoreAnnotations.SentimentClass.class);
            System.out.println(sentimentName + "\t" + sentimentInt + "\t" + sentence);
        }
    }

    /**
     * Делит отзыв по предложениям, оцениваем каждое отдельно, выводит среднее и принимает решение о настроении отзыва.
     * @param reviewFeedback - текст отзыва
     */
    public static void estimatingSentimentForReview(String reviewFeedback) {
        int reviewSum = 0;
        int reviewCount = 0;
        Annotation annotation = pipeline.process(reviewFeedback);
        for (CoreMap sentence : annotation.get(CoreAnnotations.SentencesAnnotation.class)) {
            Tree tree = sentence.get(SentimentAnnotatedTree.class);
            reviewSum += RNNCoreAnnotations.getPredictedClass(tree);
            reviewCount++;
        }

        double average = (double) reviewSum / reviewCount;
        System.out.println("Feedback: " + reviewFeedback);
        System.out.println("Average sentiment: " + FeedbackResult.getResult(average));
        estimatingSentiment(reviewFeedback);
    }
}
