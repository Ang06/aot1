package com.plutonii.masters.degre.l1;

import com.plutonii.masters.degre.l1.service.ApartmentReviewService;

public class Main {
    public static void main(String[] args) {
        ApartmentReviewService.analyze();
    }
}
